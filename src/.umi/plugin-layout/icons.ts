// @ts-nocheck

  import SmileOutlined from '@ant-design/icons/es/icons/SmileOutlined';
import DashboardOutlined from '@ant-design/icons/es/icons/DashboardOutlined';
import TableOutlined from '@ant-design/icons/es/icons/TableOutlined';
import FormOutlined from '@ant-design/icons/es/icons/FormOutlined';
import ProfileOutlined from '@ant-design/icons/es/icons/ProfileOutlined';
import CheckCircleOutlined from '@ant-design/icons/es/icons/CheckCircleOutlined';
import WarningOutlined from '@ant-design/icons/es/icons/WarningOutlined';
import UserOutlined from '@ant-design/icons/es/icons/UserOutlined';
import HighlightOutlined from '@ant-design/icons/es/icons/HighlightOutlined'
  export default {
    SmileOutlined,
DashboardOutlined,
TableOutlined,
FormOutlined,
ProfileOutlined,
CheckCircleOutlined,
WarningOutlined,
UserOutlined,
HighlightOutlined
  }