// @ts-nocheck

import SmileOutlined from '@ant-design/icons/SmileOutlined';
import DashboardOutlined from '@ant-design/icons/DashboardOutlined';
import TableOutlined from '@ant-design/icons/TableOutlined';
import FormOutlined from '@ant-design/icons/FormOutlined';
import ProfileOutlined from '@ant-design/icons/ProfileOutlined';
import CheckCircleOutlined from '@ant-design/icons/CheckCircleOutlined';
import WarningOutlined from '@ant-design/icons/WarningOutlined';
import UserOutlined from '@ant-design/icons/UserOutlined';
import HighlightOutlined from '@ant-design/icons/HighlightOutlined'

export default {
  SmileOutlined,
DashboardOutlined,
TableOutlined,
FormOutlined,
ProfileOutlined,
CheckCircleOutlined,
WarningOutlined,
UserOutlined,
HighlightOutlined
}
    