import React, { useRef, useState } from 'react';
import { Modal } from 'antd';
import ProTable from '@ant-design/pro-table';
import type { MediaListData } from '../data';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import { queryRule } from '../service';

interface MediaProps {
  modalVisible: boolean;
  onCancel: () => void;
  onSubmit: (value: any) => void;
}

const MediaUpload: React.FC<MediaProps> = (props) => {
  const actionRef = useRef<ActionType>();
  const [image, setImage] = useState<any[]>();

  const columns: ProColumns<MediaListData>[] = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: 'Image',
      dataIndex: 'image',
      key: 'image',
      valueType: 'image',
      hideInSearch: true,
    },
    {
      title: 'Sub Directory',
      dataIndex: 'sub_directory',
      key: 'sub_directory',
      hideInSearch: true,
    },
    {
      title: 'Size',
      dataIndex: 'size',
      key: 'size',
      hideInSearch: true,
    },
  ];

  return (
    <Modal
      destroyOnClose
      title="Media Upload"
      visible={props.modalVisible}
      onCancel={() => {
        props.onCancel();
      }}
      onOk={() => {
        props.onSubmit(image);
        props.onCancel();
      }}
      width={'40%'}
    >
      <ProTable<MediaListData>
        headerTitle="Media"
        actionRef={actionRef}
        rowKey="id"
        columns={columns}
        search={{
          labelWidth: 'auto',
        }}
        request={(params, sorter) => queryRule({ ...params, sorter })}
        pagination={{
          pageSize: 10,
        }}
        rowSelection={{
          type: 'radio',
          onChange: (_, selectedRows) => {
            setImage(selectedRows);
          },
        }}
      />
    </Modal>
  );
};

export default MediaUpload;
