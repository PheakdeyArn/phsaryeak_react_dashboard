export interface MediaListItem {
  id: number;
  title: string;
  name: string;
  extension: string;
  type: string;
  sub_directory: string;
  size: string;
  image: string;
  date_created: string;
}

export interface QueryParams {
  name?: string;
  limit?: string | number;
  offset?: string | number;

  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
}

export interface MediaListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface MediaListData {
  list: TodoListItem[];
  pagination: Partial<MediaListPagination>;
}

export interface MediaListParams {
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[] | null>;
  sorter?: Record<string, any | null>;
}

export interface MediaUpdateParams {
  text?: string;
  checked?: boolean;
  active?: boolean;
}
