import { request } from 'umi';
import type { QueryParams } from '@/utils/data';
import { MEDIA_URL } from '@/utils/constants';

export async function queryRule(params?: QueryParams) {
  return request(MEDIA_URL, {
    method: 'GET',
    params: {
      ...params,
    },
  });
}
