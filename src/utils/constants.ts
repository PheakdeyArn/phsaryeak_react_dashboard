export const BASE_URL = 'http://localhost:8080';

export const SLIDER_URL = BASE_URL + '/slider/';
export const CATEGORY_URL = BASE_URL + '/categories/';
export const PRODUCT_URL = BASE_URL + '/product/';
export const MEDIA_URL = BASE_URL + '/media/';
export const CUSTOMER_URL = BASE_URL + '/customer/';
export const ADDRESS_URL = BASE_URL + '/address/';
export const TRANSACTION_URL = BASE_URL + '/transaction/';
