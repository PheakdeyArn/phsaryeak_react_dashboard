import React, { useEffect, useRef, useState } from 'react';
import { Button, Form, Image, Modal, Select } from 'antd';
import type { TableListItem, CategoryListItem, SelectType } from '../data';
import type { ProFormInstance } from '@ant-design/pro-form';
import ProForm, { ProFormSelect } from '@ant-design/pro-form';
import { getProducts } from '../service';

import MediaUpload from '@/utils/components/Media';

export type FormValueType = {
  target?: string;
  template?: string;
  type?: string;
  time?: string;
  frequency?: string;
  image?: string;
} & Partial<TableListItem>;

interface CreateFormProps {
  modalVisible: boolean;
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: any) => Promise<void>;
  categoryList: CategoryListItem[];
  values: Partial<TableListItem>;
  title: string;
}

const { Option } = Select;

const type: SelectType[] = [
  {
    value: 'default',
    label: 'Default',
  },
  {
    value: 'categories',
    label: 'Category',
  },
  {
    value: 'products',
    label: 'Product',
  },
];

const CreateAndUpdateForm: React.FC<CreateFormProps> = ({
  modalVisible,
  onCancel,
  onSubmit,
  values,
  title,
  categoryList,
}) => {
  const [imageUploadVisible, handleImageUploadVisible] = useState<boolean>(false);
  const [formImage, setFormImage] = useState();
  const [typeState, setTypeState] = useState<string>();
  const [form] = Form.useForm();
  const formRef = useRef<ProFormInstance>();

  useEffect(() => {
    form.setFieldsValue({
      type: values.type,
      image: values.image,
      type_id: values.type_id,
    });
    setTypeState(values.type);
    return () => {
      form.resetFields();
      setTypeState('');
    };
  }, [modalVisible]);

  useEffect(() => {
    form.setFieldsValue({
      image: formImage,
    });
  }, [formImage]);

  const onChangeType = (value: any) => {
    setTypeState(value);
    values.type = value;
  };

  const queryProducts = async (keyword: string) => {
    const selectProductList: SelectType[] = [];
    const productParams = {
      search: keyword,
    };

    const products = await getProducts({ ...productParams });
    products.data.map((value: any) => {
      selectProductList.push({ value: value.id, label: value.name });
    });
    return selectProductList;
  };

  return (
    <>
      <Modal
        width="50%"
        getContainer={false}
        destroyOnClose
        title={title}
        visible={modalVisible}
        onCancel={() => {
          form.resetFields();
          onCancel();
        }}
        footer={null}
      >
        <ProForm
          form={form}
          formRef={formRef}
          name="control-hooks"
          onFinish={onSubmit}
          initialValues={{
            type: values.type,
            type_id: values.type_id,
            image: values.image,
          }}
        >
          <Form.Item name="type" label="Type" rules={[{ required: true }]}>
            <Select
              showSearch
              allowClear
              style={{ width: 200 }}
              placeholder="Select type"
              value={values.type}
              onChange={onChangeType}
            >
              {type.map((value: SelectType) => (
                <Option key={value.value} value={value.value}>
                  {value.label}
                </Option>
              ))}
            </Select>
          </Form.Item>

          {typeState == 'categories' ? (
            <Form.Item name="type_id" label="Type ID" rules={[{ required: true }]}>
              <Select
                showSearch
                allowClear
                style={{ width: 200 }}
                placeholder="Select a category"
                value={values.type_id}
              >
                {categoryList.map((value: CategoryListItem) => (
                  <Option key={value.id} value={value.id}>
                    {value.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          ) : null}

          {typeState == 'products' ? (
            <ProFormSelect
              width="lg"
              name="type_id"
              label="Type ID"
              showSearch
              request={async ({ keyWords }) => {
                return queryProducts(keyWords);
              }}
              placeholder="Please select a product!"
              rules={[{ required: true, message: 'Please select a product!' }]}
            />
          ) : null}

          <Form.Item label="Slider Image">
            <Button
              htmlType="button"
              onClick={() => {
                handleImageUploadVisible(true);
              }}
            >
              Upload
            </Button>
          </Form.Item>

          <Form.Item name="image" rules={[{ required: true }]}>
            <Image width={200} src={values.image} />
          </Form.Item>

          {/*Media Upload for Image */}
          <MediaUpload
            modalVisible={imageUploadVisible}
            onCancel={() => handleImageUploadVisible(false)}
            onSubmit={async (value: any[]) => {
              values.image = value[0].image;
              setFormImage(value[0].image);
            }}
          />
          <hr />
        </ProForm>
      </Modal>
    </>
  );
};

export default CreateAndUpdateForm;
