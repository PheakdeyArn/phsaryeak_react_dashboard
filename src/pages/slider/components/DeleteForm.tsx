import React from 'react';
import { Button, message, Modal } from 'antd';
import type { TableListItem } from '../data';
import { deleteRule } from '../service';

interface CreateFormProps {
  modalVisible: boolean;
  onCancel: () => void;
  currentRow: Partial<TableListItem>;
  onSuccess: (values: boolean) => Promise<void>;
}

/**
 * Handle Delete
 *
 * @param id
 */
const handleDelete = async (id?: number) => {
  const showMessage = message.loading('Deleting...');
  try {
    await deleteRule(id);
    showMessage();
    message.success('Delete successfully');
    return true;
  } catch (error) {
    showMessage();
    message.error('Failed to delete, please try again！');
    return false;
  }
};

const DeleteForm: React.FC<CreateFormProps> = (props) => {
  /***
   * Delete Slider
   *
   * @param id
   */
  const onDelete = async (id?: number) => {
    const success: boolean = await handleDelete(id);
    if (success) {
      await props.onSuccess(true);
      props.onCancel();
    }
  };

  return (
    <Modal
      destroyOnClose
      title="Delete Slider"
      visible={props.modalVisible}
      footer={[
        <Button danger key="submit" type="primary" onClick={() => onDelete(props.currentRow?.id)}>
          Delete
        </Button>,
      ]}
      onCancel={() => {
        props.onCancel();
      }}
    >
      <p>You won't be able to revert this! Are you sure ?</p>
    </Modal>
  );
};

export default DeleteForm;
