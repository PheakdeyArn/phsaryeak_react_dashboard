import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, message } from 'antd';
import React, { useState, useRef, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { queryRule, updateRule, addRule } from './service';
import type { TableListPagination, TableListItem, AddUpdateParams, CategoryListItem } from './data';
import DeleteForm from './components/DeleteForm';
import CreateAndUpdateForm from './components/CreateAndUpdateForm';
import { queryRule as getCategories } from '@/pages/category/category-list/service';

/**
 * Create product
 *
 * @param fields
 */
const handleAdd = async (fields: AddUpdateParams) => {
  const showProcessMsg = message.loading('Adding');

  try {
    await addRule({ ...fields });

    showProcessMsg();
    message.success('Added Successfully');
    return true;
  } catch (error) {
    showProcessMsg();
    message.error('Failed to add, please try again！');
    return false;
  }
};

/**
 * Update product
 *
 * @param fields
 * @param currentRow
 */
const handleUpdate = async (fields: AddUpdateParams, currentRow?: TableListItem) => {
  const showProcessMsg = message.loading('Updating');

  try {
    await updateRule(fields, currentRow?.id);
    showProcessMsg();
    message.success('Update Successful');
    return true;
  } catch (error) {
    showProcessMsg();
    message.error('Update failed, please try again！');
    return false;
  }
};

const TableList: React.FC = () => {
  const actionRef = useRef<ActionType>();
  const [categoriesList, setCategoriesList] = useState<CategoryListItem[]>([]);
  const [currentRow, setCurrentRow] = useState<TableListItem>();

  const [createFormVisible, handleCreateFormVisible] = useState<boolean>(false);
  const [updateFormVisible, handleUpdateFormVisible] = useState<boolean>(false);
  const [deleteModalVisible, handleDeleteModalVisible] = useState<boolean>(false);

  // Create useEffect to fetch Category
  useEffect(() => {
    (async () => {
      await getCategories().then((res) => setCategoriesList(res.data));
    })();
  }, []);

  const columns: ProColumns<TableListItem>[] = [
    {
      title: 'ID',
      dataIndex: 'id',
      sorter: (a: any, b: any) => a.id - b.id,
      hideInForm: true,
      key: 'id',
    },
    {
      title: 'Image',
      dataIndex: 'image',
      key: 'image',
      valueType: 'image',
      hideInSearch: true,
    },
    {
      title: 'Type ',
      dataIndex: 'type',
      key: 'type',
    },
    {
      title: 'Type ID',
      dataIndex: 'type_id',
      sorter: (a: any, b: any) => a.id - b.id,
      key: 'type_id',
    },

    {
      title: 'Action',
      dataIndex: 'option',
      key: 'Option',
      valueType: 'option',
      hideInDescriptions: true,
      hideInSearch: true,
      render: (_, record) => [
        <Button
          key="updateBtn"
          icon={<EditOutlined />}
          onClick={() => {
            setCurrentRow(record);
            handleUpdateFormVisible(true);
          }}
        />,
        <Button
          danger
          icon={<DeleteOutlined />}
          key="delete"
          onClick={() => {
            handleDeleteModalVisible(true);
            setCurrentRow(record);
          }}
        />,
      ],
    },
  ];

  return (
    <>
      <PageContainer>
        <ProTable<TableListItem, TableListPagination>
          columns={columns}
          headerTitle="Product Table"
          actionRef={actionRef}
          rowKey="id"
          search={false}
          toolBarRender={() => [
            <Button
              key="Add-Product-Form"
              type="primary"
              onClick={() => {
                handleCreateFormVisible(true);
              }}
            >
              <PlusOutlined /> Add New
            </Button>,
          ]}
          request={async (params, sorter) => queryRule({ ...params, sorter })}
        />

        {/* Create Form */}
        <CreateAndUpdateForm
          title="Add New Slider"
          onCancel={() => {
            handleCreateFormVisible(false);
            setCurrentRow(undefined);
          }}
          modalVisible={createFormVisible}
          values={currentRow || {}}
          categoryList={categoriesList as CategoryListItem[]}
          onSubmit={async (value: any) => {
            const success = await handleAdd(value);

            if (success) {
              handleCreateFormVisible(false);
              setCurrentRow(undefined);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
        />

        {/* Update Form */}
        <CreateAndUpdateForm
          title="Update Slider"
          onCancel={() => {
            handleUpdateFormVisible(false);
            setCurrentRow(undefined);
          }}
          modalVisible={updateFormVisible}
          values={currentRow || {}}
          categoryList={categoriesList as [CategoryListItem]}
          onSubmit={async (value: any) => {
            const success = await handleUpdate(value, currentRow);

            if (success) {
              handleUpdateFormVisible(false);
              setCurrentRow(undefined);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
        />

        {/* Delete Modal */}
        <DeleteForm
          onCancel={() => handleDeleteModalVisible(false)}
          modalVisible={deleteModalVisible}
          currentRow={currentRow || {}}
          onSuccess={async (value: boolean) => {
            if (value) {
              handleDeleteModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
        />
      </PageContainer>
    </>
  );
};

export default TableList;
