export interface TableListItem {
  id: number;
  type: string;
  type_id: number;
  image: string;
  date_added: string;
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface QueryParams {
  limit?: string | number;
  offset?: string | number;

  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
}

export interface AddUpdateParams {
  type?: string;
  type_id?: number;
  image?: string;
}

export interface CategoryListItem {
  id: number;
  name: string;
  slug?: string;
  banner?: string;
  row_order?: number;
  status: number;
  parent: number;
  image?: string;
}

export interface ProductParams {
  search?: string;
  get_all_products?: string;
}

export interface SelectType {
  value: number | string;
  label: string;
}
