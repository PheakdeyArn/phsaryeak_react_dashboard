import { request } from 'umi';
import type { TableListItem, QueryParams, AddUpdateParams, ProductParams } from './data';
import { SLIDER_URL, PRODUCT_URL } from '@/utils/constants';

export async function queryRule(params?: QueryParams) {
  return request<{
    data: TableListItem[];
    total?: number;
    error?: boolean;
  }>(SLIDER_URL, {
    method: 'GET',
    params: {
      ...params,
    },
  });
}

export async function addRule(params: AddUpdateParams) {
  return request(SLIDER_URL, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

export async function updateRule(params: AddUpdateParams, id?: number) {
  return request(SLIDER_URL + `${id}/`, {
    method: 'PATCH',
    data: {
      ...params,
    },
  });
}

export async function deleteRule(id?: number) {
  return request(SLIDER_URL + `${id}/`, {
    method: 'DELETE',
  });
}

export async function getProducts(params?: ProductParams) {
  return request<{
    data: any;
    total?: number;
    error?: boolean;
  }>(PRODUCT_URL, {
    method: 'GET',
    params: {
      ...params,
    },
  });
}
