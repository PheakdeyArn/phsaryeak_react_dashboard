import { request } from 'umi';
import type { TableListItem, QueryParams } from './data';
import { TRANSACTION_URL } from '@/utils/constants';

export async function queryRule(params?: QueryParams) {
  return request<{
    data: TableListItem[];
    total?: number;
    error?: boolean;
  }>(TRANSACTION_URL, {
    method: 'GET',
    params: {
      ...params,
    },
  });
}
