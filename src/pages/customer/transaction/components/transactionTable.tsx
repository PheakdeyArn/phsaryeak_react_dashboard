import React, { useRef, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { queryRule } from '@/pages/customer/transaction/service';
import type { TableListPagination, TableListItem } from '@/pages/customer/transaction/data';

interface TransactionProps {
  user_id?: number;
  transaction_type_props?: string;
  columns: any[];
  searchable?: boolean;
}

const Transaction: React.FC<TransactionProps> = ({
  user_id,
  columns,
  transaction_type_props,
  searchable = true,
}) => {
  const actionRef = useRef<ActionType>();

  useEffect(() => {
    actionRef.current?.reload();
  }, [user_id]);

  const getList = async (params: any) => {
    const request_params = {
      ...params,
    };

    if (user_id !== undefined) {
      request_params.user_id = user_id;
    }

    if (transaction_type_props !== undefined) {
      request_params.transaction_type = transaction_type_props;
    }

    return await queryRule({ ...request_params });
  };

  return (
    <>
      <PageContainer>
        <ProTable<TableListItem, TableListPagination>
          columns={columns}
          headerTitle="Address Table"
          actionRef={actionRef}
          rowKey="id"
          search={searchable === false ? false : { labelWidth: 120 }}
          request={async (params, sorter) => getList({ ...params, sorter })}
        />
      </PageContainer>
    </>
  );
};

export default Transaction;
