import React from 'react';
import { TRANSACTION_COLUMNS } from '@/pages/customer/transaction/constants';
import Transaction from '@/pages/customer/transaction/components/transactionTable';

const TransactionTable: React.FC = () => {
  return <Transaction columns={TRANSACTION_COLUMNS} transaction_type_props={'transaction'} />;
};

export default TransactionTable;
