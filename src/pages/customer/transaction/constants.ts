import type { ProColumns } from '@ant-design/pro-table';
import type { TableListItem } from '@/pages/customer/transaction/data';

const user_id_row = {
  title: 'User ID',
  dataIndex: 'user_id',
  hideInTable: true,
  hideInForm: true,
  key: 'user_id',
};

const id_row = {
  title: 'ID',
  dataIndex: 'id',
  key: 'id',
  hideInSearch: true,
  sorter: (a: any, b: any) => a.id - b.id,
  hideInForm: true,
};

const username_row = {
  title: 'User Name',
  dataIndex: 'username',
  hideInSearch: true,
  key: 'username',
  formItemProps: {
    rules: [
      {
        required: true,
        message: 'Checked is required',
      },
    ],
  },
};

const order_id_row = {
  title: 'Order ID',
  dataIndex: 'order',
  hideInSearch: true,
  key: 'order',
};

const transaction_id_row = {
  title: 'Transaction ID',
  dataIndex: 'txn_id',
  hideInForm: true,
  hideInSearch: true,
  key: 'txn_id',
};

const type_row = {
  title: 'Type',
  dataIndex: 'type',
  hideInForm: true,
  hideInSearch: true,
  key: 'type',
};

const amount_row = {
  title: 'Amount',
  dataIndex: 'amount',
  hideInForm: true,
  hideInSearch: true,
  key: 'amount',
};

const status_row = {
  title: 'status',
  dataIndex: 'status',
  hideInForm: true,
  hideInSearch: true,
  key: 'status',
};

const date_created_row = {
  title: 'date_created',
  dataIndex: 'date_created',
  hideInForm: true,
  hideInSearch: true,
  key: 'date_created',
};

const message_row = {
  title: 'Message',
  dataIndex: 'message',
  hideInForm: true,
  hideInSearch: true,
  key: 'message',
};

const search_start_date = {
  /**
   * For filter Start Date
   */
  title: 'Start Date',
  dataIndex: 'start_date',
  sorter: (a: any, b: any) => a.user - b.user,
  hideInForm: true,
  hideInTable: true,
  key: 'start_date',
  valueType: 'date',
};

const search_end_date = {
  /**
   * For filter End Date
   */
  title: 'End Date',
  dataIndex: 'end_date',
  sorter: (a: any, b: any) => a.user - b.user,
  hideInForm: true,
  hideInTable: true,
  key: 'end_date',
  valueType: 'date',
};

export const TRANSACTION_COLUMNS: ProColumns<TableListItem>[] = [
  user_id_row,
  search_start_date,
  search_end_date,
  id_row,
  username_row,
  order_id_row,
  transaction_id_row,
  type_row,
  amount_row,
  status_row,
  date_created_row,
];

export const WALLET_TRANSACTION_COLUMNS: ProColumns<TableListItem>[] = [
  user_id_row,
  search_start_date,
  search_end_date,
  id_row,
  username_row,
  order_id_row,
  type_row,
  amount_row,
  status_row,
  message_row,
  date_created_row,
];
