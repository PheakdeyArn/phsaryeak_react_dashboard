export interface TableListItem {
  id: number;
  user: number;
  username: string;
  order: number | string;
  amount: number;
  transaction_type: string;
  txn_id: string;
  type: string;
  currency_code: string;
  payer_email: string;
  message: string;
  status: string;
  created_at: string;
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface QueryParams {
  username?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
}
