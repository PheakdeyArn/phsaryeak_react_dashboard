import { request } from 'umi';
import type { TableListItem, QueryParams, UpdateParams } from './data';
import { CUSTOMER_URL } from '@/utils/constants';

export async function queryRule(params?: QueryParams) {
  return request<{
    data: TableListItem[];
    total?: number;
    error?: boolean;
  }>(CUSTOMER_URL, {
    method: 'GET',
    params: {
      ...params,
    },
  });
}

export async function updateRule(params: UpdateParams, id?: number) {
  return request(CUSTOMER_URL + `${id}/`, {
    method: 'PATCH',
    data: {
      ...params,
    },
  });
}

// export async function deleteRule(id?: number) {
//   return request(CATEGORY_URL + `${id}/`, {
//     method: 'DELETE',
//   });
// }
