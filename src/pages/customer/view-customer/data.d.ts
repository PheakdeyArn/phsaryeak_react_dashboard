export interface TableListItem {
  id: number;
  username: string;
  email: string;
  mobile: string;
  balance: string;
  street: string;
  area: string;
  city: string;
  active: number;
  created_at: string;
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface QueryParams {
  username?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
}

export interface UpdateParams {
  active?: number;
}
