import { AuditOutlined, DollarOutlined } from '@ant-design/icons';
import { Button, message, Drawer, Tag, Modal, Switch, Tooltip } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import type { ProDescriptionsItemProps } from '@ant-design/pro-descriptions';
import ProDescriptions from '@ant-design/pro-descriptions';
import { queryRule, updateRule } from './service';
import type { TableListPagination, TableListItem } from './data';
import type { UpdateParams } from './data';
import AddressTable from '@/pages/customer/address/index';

import Transaction from '@/pages/customer/transaction/components/transactionTable';

import { TRANSACTION_COLUMNS } from '@/pages/customer/transaction/constants';

/**
 * Update Category
 *
 * @param fields,
 * @param id,
 */
const handleUpdate = async (fields: UpdateParams, id: number) => {
  const alertMessage = message.loading('Configuring');

  try {
    await updateRule(fields, id);
    alertMessage();

    message.success('Configuration is successful');
    return true;
  } catch (error) {
    alertMessage();
    message.error('Configuration failed, please try again！');
    return false;
  }
};

const CustomerTable: React.FC = () => {
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<TableListItem>();
  const [showDetail, setShowDetail] = useState<boolean>(false);

  const [viewAddressVisible, setViewAddressVisible] = useState(false);
  const [viewTransactionVisible, setViewTransactionVisible] = useState(false);

  const getList = async (params: any) => {
    const response = await queryRule({ ...params });
    return response;
  };

  const onChangeStatus = async (record: any, checked: boolean) => {
    const req_active = checked == true ? 1 : 0;
    const params = {
      active: req_active,
    };

    const success = await handleUpdate(params, record);
    if (success) {
      setCurrentRow(undefined);
      if (actionRef.current) {
        actionRef.current.reload();
      }
    }
  };

  const columns: ProColumns<TableListItem>[] = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      hideInSearch: true,
      sorter: (a: any, b: any) => a.id - b.id,
      hideInForm: true,
    },
    {
      title: 'Name',
      dataIndex: 'username',
      key: 'username',
      formItemProps: {
        rules: [
          {
            required: true,
            message: 'Checked is required',
          },
        ],
      },
      render: (dom, entity) => {
        return (
          <a
            onClick={() => {
              setCurrentRow(entity);
              setShowDetail(true);
            }}
          >
            {dom}
          </a>
        );
      },
    },
    {
      title: 'Email',
      dataIndex: 'email',
      hideInForm: true,
      hideInSearch: true,
      key: 'email',
    },
    {
      title: 'Mobile',
      dataIndex: 'mobile',
      key: 'mobile',
      hideInSearch: true,
    },
    {
      title: 'Balance',
      dataIndex: 'balance',
      key: 'balance',
      hideInSearch: true,
    },
    {
      title: 'Street',
      dataIndex: 'street',
      key: 'street',
      hideInSearch: true,
    },
    {
      title: 'Area',
      dataIndex: 'area',
      key: 'area',
      hideInSearch: true,
    },
    {
      title: 'City',
      dataIndex: 'city',
      key: 'city',
      hideInSearch: true,
    },
    {
      title: 'Date',
      dataIndex: 'created_at',
      key: 'created_at',
      hideInSearch: true,
    },
    {
      title: 'Status',
      dataIndex: 'active',
      sorter: (a: any, b: any) => a.active - b.active,
      key: 'active',
      hideInSearch: true,
      renderText: (val: number) => {
        if (val == 0) return <Tag color={'volcano'}> Inactive </Tag>;
        return <Tag color={'green'}> Active </Tag>;
      },
    },
    {
      title: 'Action',
      dataIndex: 'option',
      key: 'Option',
      valueType: 'option',
      hideInDescriptions: true,
      hideInSearch: true,
      render: (_, record) => [
        <Tooltip key={'view-transaction-tooltip'} title="View Transactions">
          <Button
            key="view_transaction"
            icon={<DollarOutlined />}
            onClick={() => {
              setCurrentRow(record);
              setViewTransactionVisible(true);
            }}
          />
        </Tooltip>,
        <Tooltip key={'view-address-tooltip'} title="View Address">
          <Button
            key="view_address"
            icon={<AuditOutlined />}
            onClick={() => {
              setCurrentRow(record);
              setViewAddressVisible(true);
            }}
          />
        </Tooltip>,
        <Tooltip key={'update-status-tooltip'} title="Update Status">
          <Switch
            key="update-status"
            defaultChecked={record.active == 1 ? true : false}
            onChange={async (checked) => {
              setCurrentRow(record);
              await onChangeStatus(record.id, checked);
            }}
          />
        </Tooltip>,
      ],
    },
  ];

  return (
    <>
      <PageContainer>
        <ProTable<TableListItem, TableListPagination>
          columns={columns}
          headerTitle="Customer Table"
          actionRef={actionRef}
          rowKey="id"
          search={{
            labelWidth: 120,
          }}
          request={async (params, sorter) => getList({ ...params, sorter })}
        />

        <Modal
          title="View Address"
          key={'address-modal'}
          visible={viewAddressVisible}
          onOk={() => {
            setCurrentRow(undefined);
            setViewAddressVisible(false);
          }}
          width={'80%'}
          onCancel={() => {
            setCurrentRow(undefined);
            setViewAddressVisible(false);
          }}
        >
          <AddressTable customer_id={currentRow?.id} searchable={false} />
        </Modal>

        <Modal
          title="View Transaction"
          key={'transaction-modal'}
          visible={viewTransactionVisible}
          onOk={() => {
            setCurrentRow(undefined);
            setViewTransactionVisible(false);
          }}
          width={'80%'}
          onCancel={() => {
            setCurrentRow(undefined);
            setViewTransactionVisible(false);
          }}
        >
          <Transaction user_id={currentRow?.id} columns={TRANSACTION_COLUMNS} searchable={false} />
        </Modal>

        <Drawer
          width={600}
          visible={showDetail}
          onClose={() => {
            setCurrentRow(undefined);
            setShowDetail(false);
          }}
        >
          {currentRow?.username && (
            <ProDescriptions<TableListItem>
              column={2}
              title={currentRow?.username}
              request={async () => ({
                data: currentRow || {},
              })}
              params={{
                id: currentRow?.username,
              }}
              columns={columns as ProDescriptionsItemProps<TableListItem>[]}
            />
          )}
        </Drawer>
      </PageContainer>
    </>
  );
};

export default CustomerTable;
