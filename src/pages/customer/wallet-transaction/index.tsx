import React from 'react';
import { WALLET_TRANSACTION_COLUMNS } from '@/pages/customer/transaction/constants';
import Transaction from '@/pages/customer/transaction/components/transactionTable';

const WalletTransactionTable: React.FC = () => {
  return <Transaction columns={WALLET_TRANSACTION_COLUMNS} transaction_type_props={'wallet'} />;
};

export default WalletTransactionTable;
