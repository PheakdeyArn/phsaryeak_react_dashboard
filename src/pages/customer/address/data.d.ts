export interface TableListItem {
  id: number;
  name: string;
  type: string;
  mobile: string;
  alternate_mobile: string;
  landmark: string;
  area: string;
  city: string;
  state: number;
  pincode: number;
  country: number;
  created_at: string;
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface QueryParams {
  username?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
}
