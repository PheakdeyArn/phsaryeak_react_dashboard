import React, { useRef, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { queryRule } from './service';
import type { TableListPagination, TableListItem } from './data';

interface AddressProps {
  customer_id?: number;
  searchable?: boolean;
}

const AddressTable: React.FC<AddressProps> = ({ customer_id, searchable = true }) => {
  const actionRef = useRef<ActionType>();

  useEffect(() => {
    actionRef.current?.reload();
  }, [customer_id]);

  const getList = async (params: any) => {
    const request_params = {
      ...params,
      user_id: customer_id,
    };
    return await queryRule(request_params);
  };

  const columns: ProColumns<TableListItem>[] = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      hideInSearch: true,
      sorter: (a: any, b: any) => a.id - b.id,
      hideInForm: true,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      formItemProps: {
        rules: [
          {
            required: true,
            message: 'Checked is required',
          },
        ],
      },
    },
    {
      title: 'Type',
      dataIndex: 'type',
      hideInForm: true,
      key: 'type',
      valueType: 'select',
      valueEnum: {
        Home: { text: 'Home' },
        Office: { text: 'Office' },
        Other: { text: 'Other' },
      },
    },
    {
      title: 'Mobile',
      dataIndex: 'mobile',
      key: 'mobile',
      hideInSearch: true,
    },
    {
      title: 'Alternate Mobile',
      dataIndex: 'alternate_mobile',
      key: 'alternate_mobile',
      hideInSearch: true,
    },
    {
      title: 'Land Mark',
      dataIndex: 'landmark',
      key: 'landmark',
      hideInSearch: true,
    },
    {
      title: 'Area',
      dataIndex: 'area',
      key: 'area',
      hideInSearch: true,
    },
    {
      title: 'City',
      dataIndex: 'city',
      key: 'city',
      hideInSearch: true,
    },
    {
      title: 'State',
      dataIndex: 'state',
      key: 'state',
    },
    {
      title: 'PinCode',
      dataIndex: 'pincode',
      key: 'pincode',
      hideInSearch: true,
    },
    {
      title: 'Country',
      dataIndex: 'country',
      key: 'country',
      hideInSearch: true,
    },
  ];

  return (
    <>
      <PageContainer>
        <ProTable<TableListItem, TableListPagination>
          columns={columns}
          headerTitle="Address Table"
          actionRef={actionRef}
          rowKey="id"
          search={searchable === false ? false : { labelWidth: 120 }}
          request={async (params, sorter) => getList({ ...params, sorter })}
        />
      </PageContainer>
    </>
  );
};

export default AddressTable;
