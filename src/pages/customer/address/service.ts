import { request } from 'umi';
import type { TableListItem, QueryParams } from './data';
import { ADDRESS_URL } from '@/utils/constants';

export async function queryRule(params?: QueryParams) {
  return request<{
    data: TableListItem[];
    total?: number;
    error?: boolean;
  }>(ADDRESS_URL, {
    method: 'GET',
    params: {
      ...params,
    },
  });
}
