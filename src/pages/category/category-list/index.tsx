import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, message, Drawer, Tag } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import type { ProDescriptionsItemProps } from '@ant-design/pro-descriptions';
import ProDescriptions from '@ant-design/pro-descriptions';
import { addRule, queryRule, updateRule } from './service';
import type { TableListPagination, TableListItem, AddParams } from './data';
import DeleteCategory from './components/DeleteCategory';
import CreateAndUpdateForm from '@/pages/category/category-list/components/CreateAndUpdateForm';

/**
 * Create Category
 *
 * @param fields
 */
const handleAdd = async (fields: AddParams) => {
  const alertMessage = message.loading('Adding');
  try {
    await addRule({ ...fields });
    alertMessage();
    message.success('Added successfully');
    return true;
  } catch (error) {
    alertMessage();
    message.error('Failed to add, please try again！');
    return false;
  }
};

/**
 * Update Category
 *
 * @param fields
 */
const handleUpdate = async (fields: TableListItem, currentRow?: TableListItem) => {
  const alertMessage = message.loading('Configuring');

  try {
    await updateRule(fields, currentRow?.id);
    alertMessage();

    message.success('Configuration is successful');
    return true;
  } catch (error) {
    alertMessage();
    message.error('Configuration failed, please try again！');
    return false;
  }
};

const TableList: React.FC = () => {
  const actionRef = useRef<ActionType>();
  const [recordList, setRecordList] = useState<TableListItem[]>([]);
  const [currentRow, setCurrentRow] = useState<TableListItem>();

  const [createFormVisible, handleCreateFormVisible] = useState<boolean>(false);
  const [updateFormVisible, handleUpdateFormVisible] = useState<boolean>(false);
  const [deleteModalVisible, handleDeleteModalVisible] = useState<boolean>(false);
  const [showDetail, setShowDetail] = useState<boolean>(false);

  const getList = async () => {
    const listCate = await queryRule();
    setRecordList(listCate.data);
    return listCate;
  };

  const columns: ProColumns<TableListItem>[] = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      formItemProps: {
        rules: [
          {
            required: true,
            message: 'Checked is required',
          },
        ],
      },
      render: (dom, entity) => {
        return (
          <a
            onClick={() => {
              setCurrentRow(entity);
              setShowDetail(true);
            }}
          >
            {dom}
          </a>
        );
      },
    },
    {
      title: 'Image',
      dataIndex: 'image',
      key: 'image',
      valueType: 'image',
      hideInSearch: true,
    },
    {
      title: 'Banner',
      dataIndex: 'banner',
      key: 'banner',
      valueType: 'image',
      hideInSearch: true,
    },
    {
      title: 'ID',
      dataIndex: 'id',
      sorter: (a: any, b: any) => a.id - b.id,
      hideInForm: true,
      key: 'id',
    },
    {
      title: 'Parent ID',
      dataIndex: 'parent',
      key: 'parent',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: 'Row Order',
      dataIndex: 'row_order',
      sorter: (a: any, b: any) => a.row_order - b.row_order,
      key: 'row_order',
      hideInSearch: true,
    },
    {
      title: 'Status',
      dataIndex: 'status',
      sorter: (a: any, b: any) => a.status - b.status,
      key: 'status',
      hideInSearch: true,
      renderText: (val: number) => {
        if (val == 0) return <Tag color={'volcano'}> Inactive </Tag>;
        return <Tag color={'green'}> Active </Tag>;
      },
    },
    {
      title: 'Action',
      dataIndex: 'option',
      key: 'Option',
      valueType: 'option',
      hideInDescriptions: true,
      hideInSearch: true,
      render: (_, record) => [
        <Button
          key="updateBtn"
          icon={<EditOutlined />}
          onClick={() => {
            setCurrentRow(record);
            handleUpdateFormVisible(true);
          }}
        />,
        <Button
          danger
          icon={<DeleteOutlined />}
          key="delete"
          onClick={() => {
            handleDeleteModalVisible(true);
            setCurrentRow(record);
          }}
        />,
      ],
    },
  ];

  return (
    <>
      <PageContainer>
        <ProTable<TableListItem, TableListPagination>
          columns={columns}
          headerTitle="Category Table"
          actionRef={actionRef}
          rowKey="id"
          search={{
            labelWidth: 120,
          }}
          toolBarRender={() => [
            <Button
              key="test add form"
              type="primary"
              onClick={() => {
                handleCreateFormVisible(true);
              }}
            >
              <PlusOutlined /> Add New
            </Button>,
          ]}
          request={getList}
        />

        {/* Create Form */}
        <CreateAndUpdateForm
          title="Add New Category"
          onCancel={() => {
            handleCreateFormVisible(false);
            setCurrentRow(undefined);
          }}
          modalVisible={createFormVisible}
          values={currentRow || {}}
          data={recordList as [TableListItem]}
          onSubmit={async (value) => {
            const success = await handleAdd(value);
            if (success) {
              handleCreateFormVisible(false);
              setCurrentRow(undefined);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
        />

        {/* Update Form */}
        <CreateAndUpdateForm
          title="Update Category"
          onCancel={() => {
            handleUpdateFormVisible(false);
            setCurrentRow(undefined);
          }}
          modalVisible={updateFormVisible}
          values={currentRow || {}}
          data={recordList as [TableListItem]}
          onSubmit={async (value) => {
            const success = await handleUpdate(value, currentRow);
            if (success) {
              handleUpdateFormVisible(false);
              setCurrentRow(undefined);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
        />

        {/* Delete Modal */}
        <DeleteCategory
          onCancel={() => handleDeleteModalVisible(false)}
          modalVisible={deleteModalVisible}
          currentRow={currentRow || {}}
          onSuccess={async (value: boolean) => {
            if (value) {
              handleDeleteModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
        />

        <Drawer
          width={600}
          visible={showDetail}
          onClose={() => {
            setCurrentRow(undefined);
            setShowDetail(false);
          }}
        >
          {currentRow?.name && (
            <ProDescriptions<TableListItem>
              column={2}
              title={currentRow?.name}
              request={async () => ({
                data: currentRow || {},
              })}
              params={{
                id: currentRow?.name,
              }}
              columns={columns as ProDescriptionsItemProps<TableListItem>[]}
            />
          )}
        </Drawer>
      </PageContainer>
    </>
  );
};

export default TableList;
