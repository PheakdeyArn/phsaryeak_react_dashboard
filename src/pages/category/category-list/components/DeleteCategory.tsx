import React from 'react';
import { Button, Card, message, Modal } from 'antd';
import type { TableListItem } from '../data';
import { deleteRule } from '../service';

interface CreateFormProps {
  modalVisible: boolean;
  onCancel: () => void;
  currentRow: Partial<TableListItem>;
  onSuccess: (values: boolean) => Promise<void>;
}

/**
 * Handle Delete Category
 *
 * @param id
 */
const handleDelete = async (id?: number) => {
  const alertMessage = message.loading('Deleting...');
  try {
    await deleteRule(id);
    alertMessage();
    message.success('Category deleted successfully');
    return true;
  } catch (error) {
    alertMessage();
    message.error('Failed to delete, please try again！');
    return false;
  }
};

const DeleteCategory: React.FC<CreateFormProps> = (props) => {
  /***
   * Delete Category functions
   *
   * @param id
   */
  const onDelete = async (id?: number) => {
    const success: boolean = await handleDelete(id);
    if (success) {
      await props.onSuccess(true);
      props.onCancel();
    }
  };

  return (
    <Modal
      destroyOnClose
      title="Delete Category"
      visible={props.modalVisible}
      footer={[
        <Button danger key="submit" type="primary" onClick={() => onDelete(props.currentRow?.id)}>
          Delete
        </Button>,
      ]}
      onCancel={() => {
        props.onCancel();
      }}
    >
      <p>You won't be able to revert this! Are you sure ?</p>
      <Card
        style={{ width: 240, marginTop: 16 }}
        cover={<img src={props.currentRow.image} />}
        bordered={false}
        title={<b>{props.currentRow.name}</b>}
      />
    </Modal>
  );
};

export default DeleteCategory;
