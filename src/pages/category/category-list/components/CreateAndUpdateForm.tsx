import React, { useEffect, useState } from 'react';
import { Button, Form, Image, InputNumber, Modal, Select } from 'antd';
import type { TableListItem } from '../data';
import MediaUpload from '@/utils/components/Media';
import { UploadOutlined } from '@ant-design/icons';
import ProForm, { ProFormText } from '@ant-design/pro-form';

export type FormValueType = {
  target?: string;
  template?: string;
  type?: string;
  time?: string;
  frequency?: string;
  image?: string;
} & Partial<TableListItem>;

interface CreateFormProps {
  modalVisible: boolean;
  onCancel: (flag?: boolean, formVals?: FormValueType) => void;
  onSubmit: (values: any) => Promise<void>;
  data: [TableListItem];
  values: Partial<TableListItem>;
  title: string;
}

const { Option } = Select;
const formStatus = [
  {
    label: 'Inactive',
    value: 0,
  },
  {
    label: 'Active',
    value: 1,
  },
];

const CreateAndUpdateForm: React.FC<CreateFormProps> = ({
  modalVisible,
  onCancel,
  onSubmit,
  data,
  values,
  title,
}) => {
  const [imageUploadVisible, handleImageUploadVisible] = useState<boolean>(false);
  const [bannerUploadVisible, handleBannerUploadVisible] = useState<boolean>(false);

  const [form] = Form.useForm();
  const [formImage, setFormImage] = useState();
  const [formBanner, setFormBanner] = useState();

  useEffect(() => {
    form.setFieldsValue({
      name: values.name,
      image: values.image,
      banner: values.banner,
      parent: values.parent,
      row_order: values.row_order,
      status: values.status,
    });
    return () => {
      form.resetFields();
    };
  }, [modalVisible]);

  useEffect(() => {
    form.setFieldsValue({
      image: formImage,
    });
  }, [formImage]);

  useEffect(() => {
    form.setFieldsValue({
      banner: formBanner,
    });
  }, [formBanner]);

  return (
    <>
      <Modal
        getContainer={false}
        destroyOnClose
        title={title}
        visible={modalVisible}
        onCancel={() => {
          form.resetFields();
          onCancel();
        }}
        footer={null}
        width={'40%'}
      >
        <ProForm
          form={form}
          name="control-hooks"
          onFinish={onSubmit}
          initialValues={{
            name: values.name,
            image: values.image,
            banner: values.banner,
            parent: values.parent,
            row_order: values.row_order,
            status: values.status,
          }}
        >
          <ProForm.Group>
            <ProFormText
              width="lg"
              name="name"
              label="Name "
              placeholder="Please enter name "
              rules={[{ required: true }]}
            />
          </ProForm.Group>

          <ProForm.Group>
            <Form.Item name="status" label="Status" rules={[{ required: true }]}>
              <Select
                showSearch
                allowClear
                style={{ width: 200 }}
                placeholder="Select status"
                value={values.status}
              >
                {formStatus.map((d) => (
                  <Option key={d.label} value={d.value}>
                    {d.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item name="row_order" label="Row Order" rules={[{ required: true }]}>
              <InputNumber min={0} />
            </Form.Item>
          </ProForm.Group>

          <ProForm.Group>
            <Form.Item name="parent" label="Parent">
              <Select
                showSearch
                allowClear
                style={{ width: 200 }}
                placeholder="Select a parent"
                value={values.parent}
              >
                {data.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </ProForm.Group>

          <Form.Item label="Image">
            <Button
              htmlType="button"
              icon={<UploadOutlined />}
              onClick={() => {
                handleImageUploadVisible(true);
              }}
            >
              Upload
            </Button>
          </Form.Item>

          <Form.Item name="image" rules={[{ required: true }]}>
            <Image width={200} src={values.image} />
          </Form.Item>

          <Form.Item label="Banner">
            <Button
              htmlType="button"
              icon={<UploadOutlined />}
              onClick={() => {
                handleBannerUploadVisible(true);
              }}
            >
              Upload
            </Button>
          </Form.Item>
          <Form.Item name="banner">
            <Image width={200} src={values.banner} />
          </Form.Item>
          <hr />
        </ProForm>
      </Modal>

      {/* Media Upload for Image */}
      <MediaUpload
        onCancel={() => handleImageUploadVisible(false)}
        modalVisible={imageUploadVisible}
        onSubmit={async (value: any[]) => {
          values.image = value[0].image;
          setFormImage(value[0].image);
        }}
      />

      {/* Media Upload for Banner */}
      <MediaUpload
        onCancel={() => handleBannerUploadVisible(false)}
        modalVisible={bannerUploadVisible}
        onSubmit={async (value: any[]) => {
          values.banner = value[0].image;
          setFormBanner(value[0].image);
        }}
      />
    </>
  );
};

export default CreateAndUpdateForm;
