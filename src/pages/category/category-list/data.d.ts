export interface TableListItem {
  id: number;
  name: string;
  slug?: string;
  banner?: string;
  row_order?: number;
  status: number;
  parent: number;
  image?: string;
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface QueryParams {
  has_child_or_item?: string;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
}

export interface AddParams {
  name?: string;
  slug?: string;
  image?: string;
  parent?: number | null;
  banner?: string | null;
  row_order?: number;
  status?: number;
}

export interface UpdateParams {
  name?: string;
  image?: string;
  banner?: string;
  row_order?: number;
  parent?: number;
  status?: number;
}

export interface MediaType {
  id: number;
  name: string;
  image: string;
  size: string;
}
