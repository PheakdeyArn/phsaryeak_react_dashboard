import { request } from 'umi';
import type { TableListItem, QueryParams, UpdateParams, AddParams } from './data';
import { CATEGORY_URL } from '@/utils/constants';

export async function queryRule(params?: QueryParams) {
  return request<{
    data: TableListItem[];
    total?: number;
    error?: boolean;
  }>(CATEGORY_URL, {
    method: 'GET',
    params: {
      ...params,
    },
  });
}

export async function updateRule(params: UpdateParams, id?: number) {
  return request(CATEGORY_URL + `${id}/`, {
    method: 'PATCH',
    data: {
      ...params,
    },
  });
}

export async function addRule(params: AddParams) {
  return request(CATEGORY_URL, {
    method: 'POST',
    data: {
      ...params,
    },
  });
}

export async function deleteRule(id?: number) {
  return request(CATEGORY_URL + `${id}/`, {
    method: 'DELETE',
  });
}
